class Singleton() {

    constructor(value: String) : this(){

        try {
            Thread.sleep(1000)
        } catch (ex: InterruptedException){
            ex.printStackTrace()
        }
        this.value=value
    }


    public lateinit var value: String

    companion object {

        private lateinit var instance: Singleton

        public fun getInstance(value: String): Singleton {
            if (!::instance.isInitialized) {
                instance = Singleton(value)
            }
            return instance
        }
    }
}