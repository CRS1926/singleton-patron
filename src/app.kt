
fun main(){
   println("Si tu ves el mismo valor, entonces singleton fue reusado (yay!) \n" +
           "si tu ves diferentes valores entonces 2 singletons han sido creados (boo!) \n\n" +
           "Sult: ")

    var singl = Singleton.getInstance("FOO")
    var singl2 = Singleton.getInstance("BAR")
    println("${singl.value}")
    println("${singl2.value}")

}